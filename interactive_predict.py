import sys
from gensim.models import Word2Vec
import logging
import numpy as np
from collections import defaultdict as dd
from training_transMat import get_lexicalizations

def most_similar_vector(self, vectenter, topn=5):
	model_source.init_sims()
	dists = np.dot(self.syn0norm, vectenter)
	print 'Dists: ',dists
	if not topn:
		return dists
	best = np.argsort(dists)[::-1][:topn]
	print 'best -> ',best
		# ignore (don't return) words from the input
	result = [(self.index2word[sim], float(dists[sim])) for sim in best]
	return result[:topn]
def top_translations(w,translation_matrix,numb=5):
	vec_tm = translation_matrix.dot(model_target[w])
	norm_tm = vec_tm/np.linalg.norm(vec_tm)
	val = most_similar_vector(model_source,norm_tm,numb)
	return val

if __name__ == '__main__':

	args = sys.argv[1:]

	if len(args) >= 3:
		matrix_file = args[0]
		embeddings_model = args[1]
		top_candidates = int(args[2])
		expanded = ''
	if len(args) == 4:
		expanded = args[3]
	if len(args) != 3 and len(args) != 4:
		sys.exit('You must provide three or four arguments. Check README file.')

	print 'Loading embeddings_model...'
	if embeddings_model.endswith('txt'):
		M = Word2Vec.load_word2vec_format(embeddings_model, binary=False)
	else:
		M = Word2Vec.load_word2vec_format(embeddings_model, binary=True)
	
	M_vocab = M.index2word
	model_source = M
	model_target = M

	matrix = np.loadtxt(matrix_file)
	if expanded == 'expanded':
		syn_lex_dict = get_lexicalizations(M_vocab)

	results = {}
	concept = ''
	while concept != 'leave':
		concept = raw_input('Enter concept name or type "leave": ')
		if concept == 'leave':
			sys.exit('Thank you')
		if expanded == 'expanded':
                        if concept not in syn_lex_dict:
                                print concept+" is not in the vocabulary"
                                continue
			expanded_list = syn_lex_dict[concept]
			print 'Concept: ',concept,' | Lexicalizations: ',expanded_list
			results.clear()
			for lex in expanded_list:
				vec = lex+'_'+concept
				res = top_translations(vec, matrix, numb=top_candidates)
				for idx,cand_score in enumerate(res):
					cand = cand_score[0]
					score = cand_score[1]
					pond_score = score/(idx+1)
					if not cand in results:
						results[cand] = pond_score
					else:
						if pond_score > results[cand]:
							results[cand] = pond_score
			sorted_res = sorted(results.items(), key=lambda x:x[1],reverse=True)		
			toprint = sorted_res[:top_candidates]
			for cand,score in toprint:
				print cand+'\t'+str(score)
		else:
                        if concept not in M_vocab:
                                print concept+" is not in the vocabulary"
			else:
                                res = top_translations(concept, matrix, numb=top_candidates)
                                for cand,score in res:
                                        print cand+'\t'+str(score)
