import sys
import ir_eval

def get_term(line):
	return line.split('\t')[0]

def get_hypernyms(line, is_gold=True):
	if is_gold == True:
		valid_hyps = line.strip().split('\t')[1:]
		return valid_hyps
	else:
		#print 'Line: ',line
		cand_hyps = [term_score.split()[0] for term_score in line.strip().split('\t')[1:]]
		return cand_hyps

if __name__ == '__main__':

	args = sys.argv[1:]

	if len(args) == 2:

		gold = args[0]
		predictions = args[1]

		fgold = open(gold, 'r')
		fpredictions = open(predictions, 'r')

		goldls = fgold.readlines()
		predls = fpredictions.readlines()

		all_scores = []
		scores_names = ['mrr', 'map', 'r-prec', 'p@1', 'p@2', 'p@3', 'p@4', 'p@5']
		outf = open(predictions+'__results.txt', 'w')
		for i in range(len(goldls)):

			goldline = goldls[i]
			predline = predls[i]

			gold_term = get_term(goldline)
			pred_term = get_term(predline)

			avg_pat1 = []
			avg_pat2 = []
			avg_pat3 = []
			avg_pat4 = []
			avg_pat5 = []
			avg_rprec = []
			rs = []

			if gold_term == pred_term:

				gold_hyps = get_hypernyms(goldline, is_gold=True)
				pred_hyps = get_hypernyms(predline, is_gold=False)
				gold_hyps_n = len(gold_hyps)
				r = [0 for i in range(5)]

				for i in range(len(pred_hyps)):
					if i <= gold_hyps_n:
						pred_hyp = pred_hyps[i]
						if pred_hyp in gold_hyps:
							r[i] = 1

				rs.append(r)
				avg_pat1.append(ir_eval.precision_at_k(r,1))
				avg_pat2.append(ir_eval.precision_at_k(r,2))
				avg_pat3.append(ir_eval.precision_at_k(r,3))
				avg_pat4.append(ir_eval.precision_at_k(r,4))
				avg_pat5.append(ir_eval.precision_at_k(r,5))
				avg_rprec.append(ir_eval.r_precision(r))

			else:
				sys.exit('Error: Term is different in gold and prediction files.')

			mrr_score_numb = ir_eval.mean_reciprocal_rank(rs)
			map_score_numb = ir_eval.mean_average_precision(rs)
			avg_pat1_numb = sum(avg_pat1)/len(avg_pat1)
			avg_pat2_numb = sum(avg_pat2)/len(avg_pat2)
			avg_pat3_numb = sum(avg_pat3)/len(avg_pat3)
			avg_pat4_numb = sum(avg_pat4)/len(avg_pat4)
			avg_pat5_numb = sum(avg_pat5)/len(avg_pat5)
			avg_rprec_numb = sum(avg_rprec)/len(avg_rprec)
			
			scores_results = [mrr_score_numb, map_score_numb, avg_rprec_numb, avg_pat1_numb, avg_pat2_numb, avg_pat3_numb, avg_pat4_numb, avg_pat5_numb]		
			all_scores.append(scores_results)

			outf.write(gold_term+'\t'+'\t'.join([name+':'+str(scores_results[idx]) for idx,name in enumerate(scores_names)])+'\n')

		outf.write('\n')

		print 'MRR: '+str(sum([score_list[0] for score_list in all_scores]) / len(all_scores))
		print 'MAP: '+str(sum([score_list[1] for score_list in all_scores]) / len(all_scores))
		print 'R-P: '+str(sum([score_list[2] for score_list in all_scores]) / len(all_scores))
		print 'P@1: '+str(sum([score_list[3] for score_list in all_scores]) / len(all_scores))
		print 'P@2: '+str(sum([score_list[4] for score_list in all_scores]) / len(all_scores))
		print 'P@3: '+str(sum([score_list[5] for score_list in all_scores]) / len(all_scores))
		print 'P@4: '+str(sum([score_list[6] for score_list in all_scores]) / len(all_scores))
		print 'P@5: '+str(sum([score_list[7] for score_list in all_scores]) / len(all_scores))
		outf.write('MRR: '+str(sum([score_list[0] for score_list in all_scores]) / len(all_scores))+'\n')
		outf.write('MAP: '+str(sum([score_list[1] for score_list in all_scores]) / len(all_scores))+'\n')
		outf.write('R-P: '+str(sum([score_list[2] for score_list in all_scores]) / len(all_scores))+'\n')
		outf.write('P@1: '+str(sum([score_list[3] for score_list in all_scores]) / len(all_scores))+'\n')
		outf.write('P@2: '+str(sum([score_list[4] for score_list in all_scores]) / len(all_scores))+'\n')
		outf.write('P@3: '+str(sum([score_list[5] for score_list in all_scores]) / len(all_scores))+'\n')
		outf.write('P@4: '+str(sum([score_list[6] for score_list in all_scores]) / len(all_scores))+'\n')
		outf.write('P@5: '+str(sum([score_list[7] for score_list in all_scores]) / len(all_scores))+'\n')

		outf.close()

	else:
		sys.exit('Argument: (1) Gold file; (2) Predictions file')