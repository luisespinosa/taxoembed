# -*- coding: utf-8 -*-
import sys
from gensim.models import Word2Vec
import logging
import numpy as np
from collections import defaultdict as dd

def get_lexicalizations(vector_vocab):
	out = dd(set)
	for item in vector_vocab:
		item_split = item.rsplit('_', 1)
		lex = item_split[0]
		syn = item_split[1]
		out[syn].add(lex)
	return out
	
def load_domains(domains_file):
	out_map = {}
	for line in open(domains_file, 'r'):
		linesplit = line.strip().split("\t")
		#print linesplit
		synset = linesplit[0]
		domain = linesplit[1].lower()
		out_map[synset] = domain
	return out_map


def load_training_data(path_training_data, expanded, domain, numb_training = None):
	out_terms = []
	out_hyps = []
	training_data_file=open(path_training_data,'r').readlines()
	print 'Training data length: ',len(training_data_file)
	if not expanded:
		for line in training_data_file:
			linesplit = line.strip().split("\t")
			term = linesplit[0]
			hyp = linesplit[1]
			if term in M_vocab and hyp in M_vocab:
				out_terms.append(M[term])
				out_hyps.append(M[hyp])
				if numb_training and numb_training == len(out_terms) and numb_training == len(out_hyps):
					print 'Trained with these many pairs: '	,len(out_terms),len(out_hyps)
					return out_terms,out_hyps
	elif expanded == 'expanded':
		for line in training_data_file:
			linesplit = line.strip().split("\t")
			term = linesplit[0]
			hyp = linesplit[1]
			if domain!="none":
				if term not in domains_dict or domains_dict[term]!=domain: continue
				if hyp not in domains_dict or domains_dict[hyp]!=domain: continue
			for term_lex in syn_lex_dict[term]:
				term_sense = term_lex+"_"+term
				if not term_sense in M_vocab:
					continue
				term_vec = M[term_sense]
				for hyp_lex in syn_lex_dict[hyp]:
					hyp_sense = hyp_lex+'_'+hyp
					if not hyp_sense in M_vocab:
						continue
					hyp_vec = M[hyp_sense]
					print 'Adding pair number: ',len(out_terms),len(out_hyps)
					out_terms.append(term_vec)
					out_hyps.append(hyp_vec)
					if numb_training and numb_training == len(out_terms) and numb_training == len(out_hyps):
						print 'Trained with these many pairs: '	,len(out_terms),len(out_hyps)
						return out_terms,out_hyps
	print 'Trained with these many pairs: '	,len(out_terms),len(out_hyps)
	return out_terms,out_hyps

if __name__ == '__main__':

	args = sys.argv[1:]

	if len(args) >= 4:
		training_data = args[0]
		sensembed = args[1]
		domains_file = args[2]
		input_domain = args[3]
		expanded = ''

	if len(args) == 5:
		expanded = args[4]
	if len(args) not in [4,5]:
		sys.exit('You must provide four or five arguments. Check README file.')
	if input_domain != 'none':
		domains_dict = load_domains(domains_file)
	
	domains=set()
	if input_domain != 'all':
		domains.add(input_domain.lower())
	else:
		domains=set(domains_dict.values())

	print 'Loading embeddings...'
	if sensembed.endswith('txt'):
		M = Word2Vec.load_word2vec_format(sensembed, binary=False)
	else:
		M = Word2Vec.load_word2vec_format(sensembed, binary=True)

	M_vocab = M.index2word
	model_source = M
	model_target = M
	print 'Model of vocabulary length of ',len(M_vocab),' loaded'

	if expanded == 'expanded':
		syn_lex_dict = get_lexicalizations(M_vocab)

	for domain in domains:
		print 'Working with domain: ',domain
		term_vecs,hyp_vecs = load_training_data(training_data, expanded, domain, numb_training=1000)
		print 'Data loaded, training transformation matrix'
		transformation_matrix = np.linalg.pinv(term_vecs).dot(hyp_vecs).T
		if domain=="none": np.savetxt('transformation_matrix_general.txt', transformation_matrix, delimiter=' ')
		else:
			print "Transformation matrix created. Saving vectors"
			np.savetxt('transformation_matrix_'+domain+'.txt', transformation_matrix, delimiter=' ')
			set_synsets_domain=set()
			if expanded=="expanded":
				vocab_domain=set()
				for synset in domains_dict:
					if domains_dict[synset] == domain:
						for lex in syn_lex_dict[synset]:
							sense = lex+"_"+synset
							set_synsets_domain.add(sense)
			else:
				for synset in domains_dict:
					if domains_dict[synset] == domain: set_synsets_domain.add(synset)
			vocab_domain=set.intersection(set(M_vocab),set_synsets_domain)
			txtfile=open('vectors_'+domain+'.txt','w')
			txtfile.write(str(len(vocab_domain))+" "+str(M.vector_size)+"\n")
			for item in vocab_domain:
				txtfile.write(item.encode('utf-8'))
				for dimension in M[item]:
					txtfile.write(" "+str(dimension))
				txtfile.write("\n")
			txtfile.close()
			print "Finished writing vectors on vectors_"+domain+".txt"